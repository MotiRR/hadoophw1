import random as rm


def switch(argument):
 switcher = {
  1: "64.242.88.10 - - [07/Mar/2019:16:05:49 -0800] \"GET /twiki/bin/edit/Main/Double_bounce_sender?topicparent=Main.ConfigurationVariables HTTP/1.1\" 401 12846",
  2: "64.242.88.10 - - [07/Mar/2019:17:13:50 -0800] \"GET /twiki/bin/edit/TWiki/DefaultPlugin?t=1078688936 HTTP/1.1\" 401 12846",
  3: "64.242.88.10 - - [07/Mar/2019:17:16:00 -0800] \"GET /twiki/bin/search/Main/?scope=topic&regex=on&search=^g HTTP/1.1\" 200 3675",
  4: "80-219-148-207.dclient.hispeed.ch - - [07/Mar/2019:19:47:36 -0800] \"OPTIONS * HTTP/1.0\" 200",
  5: "h24-70-56-49.ca.shawcable.net - - [07/Mar/2019:21:16:17 -0800] \"GET /twiki/view/Main/WebHome HTTP/1.1\" 404 300",
  6: "h24-70-56-49.ca.shawcable.net - - [07/Mar/2019:21:16:18 -0800] \"GET /favicon.ico HTTP/1.1\" 200 1078",
  7: "216-160-111-121.tukw.qwest.net - - [11/Mar/2019:20:49:38 -0800] \"GET /dccstats/stats-spam.1month.png HTTP/1.1\" 200 2662",
  8: "216-160-111-121.tukw.qwest.net - - [11/Mar/2019:20:49:38 -0800] \"GET /dccstats/stats-spam-ratio.1month.png HTTP/1.1\" 200 2064",
  9: "10.0.0.153 - - [12/Mar/2019:12:23:18 -0800] \"GET /cgi-bin/mailgraph.cgi/mailgraph_1.png HTTP/1.1\" 200 8964",
  10: "10.0.0.153 - - [12/Mar/2019:12:23:41 -0800] \"GET /dccstats/stats-spam.1month.png HTTP/1.1\" 200 2651",
  11: "10.0.0.153 - - [12/Mar/2019:12:23:41 -0800] \"GET /dccstats/stats-spam-ratio.1month.png HTTP/1.1\" 200 2023bb"
 }
 return switcher.get(argument, "Invalid")

if __name__ == "__main__":
 
  f = open('input.txt', 'w')  
  for index in range(100):
   record = switch(rm.randint(1,11))
   f.write(record + '\n')
  f.close()










