# HomeworkOne
# Задание
Задание:

1. Business logic:
Program which count average bytes per request by IP and total bytes by IP. Output is file with rows as: IP,175.5,109854. For logs like:
99.168.127.53 - -[20/May/2010:07:34:13 +0100] "GET /media/img/m-inact.gif HTTP/1.1" 200 2571 "http://www.example.com/" "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_1_3 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7E18 Safari/528.16"
67.195.114.50 - - [20/May/2010:07:35:27 +0100] "GET /post/261556/ HTTP/1.0" 404 15 "-" "Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)".

2. Output Format:
SequenceFile.

3. Additional Requirements:
Counters is used for statistics about malformed rows collection.

# Инструменты
+ Linux Ubuntu 18.04
+ InteliJ IDEA
+ JDK 1.8
+ Maven
+ Hadoop 2.8.5

# Скрипты
start.py - стартовый скрипт, который включает в себя generation.py, собирает и запускает проект.

generation.py - скрипт, для формирования файла ввода в hdfs.

# Работа с Hadoop
## Установка
[Ссылка 1](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html#Pseudo-Distributed_Operation)

[Ссылка 2](https://linuxconfig.org/how-to-install-hadoop-on-ubuntu-18-04-bionic-beaver-linux)
#### Запуск служб
```
start-dfs.sh
start-yarn.sh
```
#### Остановка служб
```
stop-dfs.sh
stop-yarn.sh
```
#### Загрузка файла
```
hadoop fs -put {path}
```
#### Чтение sequence файла
```
hadoop fs -text {path}
```
{path} - путь до файла.
# Запуск
```
hadoop jar {jar File} {inputFile ,inputFile} {outputFile}
```
##### Параметры:
-inputFile - файл для чтения данных;

-outputFile - файл для записи данных.
### Проверка сервера:
http://localhost:50070

http://localhost:8088

# Установка проекта
Открываем командную строку в папке с файлов pom.xml и в командной строке пишем
```
mvn clean install
```



