import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import ru.edu.hadoop.*;

public class WebLogReader {

    public static void main(String[] args) throws Exception {
        //this program must be used as: WebLogReader <input directories or files> [<in>] <output directory>
        //output directory must not be exist
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length < 2) {
            System.err.println("Usage: web log reader <in> [<in>...] <out>");
            System.exit(2);
        }
        Job job = Job.getInstance(conf, "web log reader");
        //set mapper/reducer etc classes for job
        job.setJarByClass(WebLogReader.class);
        job.setMapperClass(WebLogMapper.class);
        job.setReducerClass(WebLogReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(WebLogWritable.class);

        //get input
        for (int i = 0; i < otherArgs.length - 1; ++i) {
            FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
        }
        //set output directory
        FileOutputFormat.setOutputPath(job,
                new Path(otherArgs[otherArgs.length - 1]));
        //b. Output Format:
        //SequenceFile.
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        int rez = job.waitForCompletion(true) ? 0 : 1;

        System.exit(rez);
    }
}
